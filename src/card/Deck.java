package card;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Deck {
	private List<Card> cards = new ArrayList<>();
	
	
	
	
	public Deck() {
		
	}
	
	public Deck(List<Card> cards) {
		this.cards = cards;
	}
	
	
	/*
	 * Static methods.
	 */
	public static int countPip(Deck deck, Pip pip) {
		int count = 0;
		for (Card card : deck.getCards()) {
			if (card.getPip() == pip) {
				count++;
			}
		}
		return count;
	}
	
	public static int countSuit(Deck deck, Suit suit) {
		int count = 0;
		for (Card card : deck.getCards()) {
			if (card.getSuit() == suit) {
				count++;
			}
		}
		return count;
	}
	
	/*
	 * Methods.
	 */
	public void add(Card card) {
		cards.add(card);
	}
	
	public void add(List<Card> newCards) {
		cards.addAll(newCards);
	}
	
	public void clear() {
		cards.clear();
	}
	
	public Card draw() {
		return cards.remove(cards.size()-1);
	}
	
	public Card get(int i) {
		return cards.get(i);
	}
	
	public void init() {
		List<Card> l = new LinkedList<>();
		for (Pip pip : Pip.valuesRegular()) {
			for (Suit suit : Suit.valuesRegular()) {
				l.add(new Card(pip, suit));
			}
		}
		for (int i = 0; i < 4; i++) {
			l.add(new Card(Suit.WIZARD));
			l.add(new Card(Suit.JESTER));
		}
		cards = l;
		shuffle();
	}
	
	public Card remove(int i) {
		return cards.remove(i);
	}
	
	public boolean remove(Card c) {
		return cards.remove(c);
	}
	
	public void shuffle() {
		java.util.Collections.shuffle(cards);
	}
	
	public void sort(CardComparator cardComparator) {
		Collections.sort(cards, cardComparator);
	}
	
	public List<Card> getCards() {
		return cards;
	}
	
	public int getSize() {
		return cards.size();
	}
	

	@Override
	public String toString() {
		String result = "[";
		for (int i = 0; i < cards.size(); i++) {
			result += cards.get(i).toString();
			if (i != cards.size()-1) { // don't end list with a comma
				result += ",";				
			}
		}
		result += "]";
		return result;
	}
	
	
}
