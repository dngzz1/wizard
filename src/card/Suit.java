package card;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public enum Suit {
	JESTER("♘", true), 
	DIAMOND("♦", false), 
	CLUB("♣", false), 
	HEART("♥", false), 
	SPADE("♠", false), 
	WIZARD("♕", true);
	
	private final String symbol;
	private final boolean isSpecial;
	Suit(String symbol, boolean isSpecial) {
		this.symbol = symbol;
		this.isSpecial = isSpecial;
	}
	
	public static List<Suit> valuesRegular() {
		return Arrays.stream(values()).filter(c -> !c.isSpecial).collect(Collectors.toList());
	}
	
	public static Suit getRandomSuit() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	
	
	public boolean getIsSpecial() {
		return isSpecial;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
}
