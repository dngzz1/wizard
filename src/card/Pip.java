/**
 * The numerical value on a playing card.
 */
package card;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author dan5
 *
 */
public enum Pip {
	JESTER("JE", 0, true), 
	TWO("2", 2, false), 
	THREE("3", 3, false), 
	FOUR("4", 4, false), 
	FIVE("5", 5, false), 
	SIX("6", 6, false), 
	SEVEN("7", 7, false), 
	EIGHT("8", 8, false), 
	NINE("9", 9, false), 
	TEN("10", 10, false), 
	JACK("J", 11, false), 
	QUEEN("Q", 12, false), 
	KING("K", 13, false), 
	ACE("A", 14, false), 
	WIZARD("WZ", 15, true);
	
	private final String displayName;
	private final boolean isSpecial;
	private final int value;
	/*
	 * Constructor.
	 */
	Pip(String displayName, int value, boolean isSpecial) {
		this.displayName = displayName;
		this.value = value;
		this.isSpecial = isSpecial;
	}
	
	
	public static List<Pip> valuesRegular() {
		return Arrays.stream(values()).filter(c -> !c.isSpecial).collect(Collectors.toList());
	}
	
	
	
	public boolean getIsSpecial() {
		return isSpecial;
	}
	
	public static Pip getRandomPip() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public int getValue() {
		return value;
	}
}
