package card;
import static card.Suit.*;
import java.util.Comparator;

public class CardComparator implements Comparator<Card>{
	private Suit trumpSuit;
	/*
	 * Constructor.
	 */
	public CardComparator(Card trump) {
		trumpSuit = trump.getSuit();
	}
	public CardComparator(Suit suit) {
		trumpSuit = suit;
	}
	public CardComparator() {
		trumpSuit = JESTER;
	}
	
	@Override
	public int compare(Card c1, Card c2) {
		if (c1.getSuit() == WIZARD) {
			return 1;
		} else if (c2.getSuit() == WIZARD) {
			return -1;
		} else if (c1.getSuit() == JESTER) {
			return -1;
		} else if (c2.getSuit() == JESTER) {
			return 1;
		}
		if (c1.getSuit() == trumpSuit && c2.getSuit() == trumpSuit) {
			return c1.getPip().compareTo(c2.getPip());
		}
		if (c1.getSuit() == trumpSuit && c2.getSuit() != trumpSuit) {
			return 1;
		}
		if (c1.getSuit() != trumpSuit && c2.getSuit() == trumpSuit) {
			return -1;
		}
		return c1.getPip().compareTo(c2.getPip());
	}
	
	
	
}
