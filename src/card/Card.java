package card;



import player.Player;

public class Card {
	private Pip pip;
	private Suit suit;
	private Player owner;
	
	
	public Card(Pip pip, Suit suit) {
		this.pip = pip;
		// Jester and Wizard have no suit so override suit.
		if (pip == Pip.JESTER) {
			suit = Suit.JESTER;
		} else if (pip == Pip.WIZARD) {
			suit = Suit.WIZARD;
		} 
		this.suit = suit;
	}
	
	
	public Card(Suit suit) {
		this.suit = suit;
		if (suit == Suit.WIZARD) {
			this.pip = Pip.WIZARD;
		} else if (suit == Suit.JESTER) {
			this.pip = Pip.JESTER;
		} else {
			this.pip = Pip.getRandomPip();
		}
	}
	
	
	
	public Player getOwner() {
		return owner;
	}

	public Pip getPip() {
		return pip;
	}


	public Suit getSuit() {
		return suit;
	}
	
	public void setOwner(Player owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return pip.getDisplayName() + suit.getSymbol();
	}
	
	
	
	

}
