/**
 * Decides for the auto players how many tricks to bet.
 */
package compete;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import card.Card;
import card.Deck;
import card.Pip;
import card.Suit;
import main.Arena;

/**
 * @author dan5
 *
 */
public class Strategy {
	Arena arena;
	Deck cards;
	/*
	 * Two cases to factor in when deciding how many tricks to bet.
	 * 1. If there is no trump...
	 * a. count the number of large cards.
	 * 2. If there is a trump...
	 * a. count the number of large trumps.
	 * b. count the number of small trumps.
	 * c. count the number of non-trumps. 
	 */
	
	public Strategy() {

	}
	
	public void update(Arena arena, Deck cards) {
		this.arena = arena;
		this.cards = cards;
	}
	
	/*
	 * Decide how many tricks you can bet.
	 */
	public int decideTricks() {
		if (arena.getTrump() == Suit.JESTER) {
			return Deck.countPip(cards, Pip.KING) + Deck.countPip(cards, Pip.ACE) + Deck.countPip(cards, Pip.WIZARD);
		}
		return Deck.countSuit(cards, arena.getTrump()) + Deck.countSuit(cards, Suit.WIZARD);
	}
	
	/*
	 * Decide what to set as trump when asked.
	 */
	public Suit decideTrump() {
		Map<Suit, Integer> map = new HashMap<>();
		for (int i = 0; i < cards.getSize(); i++) {
			Suit suit = cards.get(i).getSuit();
			if (suit.getIsSpecial()) {
				continue;
			}
			Integer val = map.get(suit);
			map.put(suit,  val == null ? 1 : val + 1);
		}
		Entry<Suit, Integer> max = null;
		for (Entry<Suit, Integer> e : map.entrySet()) {
			if (max == null || e.getValue() > max.getValue()) {
				max = e;
			}
		}
		if (max == null) {
			return Suit.getRandomSuit();
		}
		
		return max.getKey();
	}
	
	
	/*
	 * Get random legal card to play.
	 */
	public Card playCard() {
		List<Card> regCardsInPot = arena.getPot().getCards().stream()
				.filter(c -> !c.getSuit().getIsSpecial())
				.collect(Collectors.toList());
		if (regCardsInPot.size() == 0) {
			// no regular suited card is in the pot.
			// hence no restrictions on card to be played.
			int r = ThreadLocalRandom.current().nextInt(0, cards.getSize());
			return cards.get(r);
		} 
		// else someone played a regular suited card.
		// check whether we have any of the same suit.
		Suit leadSuit = regCardsInPot.get(0).getSuit();
		List<Card> cardsOfSameSuit = cards.getCards().stream()
				.filter(c -> c.getSuit() == leadSuit)
				.collect(Collectors.toList());
		if (cardsOfSameSuit.size() == 0) {
			// we don't have the same suit, so no restrictions.
			int r = ThreadLocalRandom.current().nextInt(0, cards.getSize());
			return cards.get(r);
		}
		// else must follow suit, or play a special card.
		while(true) {
			int r = ThreadLocalRandom.current().nextInt(0, cards.getSize());
			Card c = cards.get(r);
			if (c.getSuit().getIsSpecial() || c.getSuit() == leadSuit) {
				return cards.get(r);
			}
		}
	}
	
	
}
