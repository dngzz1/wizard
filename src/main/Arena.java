package main;

import static card.Suit.JESTER;
import static card.Suit.WIZARD;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import card.Card;
import card.CardComparator;
import card.Deck;
import card.Suit;
import player.Player;
import player.PlayerUser;



public class Arena{
	// Required parameters
	private final List<Player> players;
	
	// Optional settings
	private final boolean displayEveryonesCards;
	private final boolean printToScreenIsOn;
	private final boolean makeUserPressEnter;
	
	// Automatically generated parameters
	private CardComparator cardComparator;
	private final Scanner scanner = new Scanner(System.in);
	private int dealerInt;
	private final Deck deck = new Deck();
	private boolean gameActive = true;
	private final Map<Player, Integer> id = new HashMap<>();
	private final int numPlayer; // BETWEEN 2 and 6 inclusive.
	private final Deck pot = new Deck();
	private int round = 0;
	private int starterInt;
	private int[] tricks_bets;
	private int[] tricks_actual;
	private Card trumpCard;
	private Suit trump;
	private int turn = 0;
	


	/*
	 * Constructor with builder design pattern.
	 */
	public static class Builder {
		// Required parameters
		private final List<Player> players;
		
		// Optional parameters - initialised to default values
		private boolean displayEveryonesCards = false;
		private boolean printToScreenIsOn = true;
		private boolean makeUserPressEnter = true;
		
		public Builder(List<Player> players) {
			this.players = players;
		}
		
		public Builder displayEveryonesCards(boolean val) {
			displayEveryonesCards = val;
			return this;
		}
		public Builder printToScreenIsOn(boolean val) {
			printToScreenIsOn = val;
			return this;
		}
		
		public Builder makeUserPressEnter(boolean val) {
			makeUserPressEnter = val;
			return this;
		}
		
		public Arena build() {
			return new Arena(this);
		}
	}
	
	public Arena(Builder builder) {
		players = builder.players;
		displayEveryonesCards = builder.displayEveryonesCards;
		printToScreenIsOn = builder.printToScreenIsOn;
		makeUserPressEnter = builder.makeUserPressEnter;
		numPlayer = players.size();
		for (int i = 0; i < numPlayer; i++) {
			id.put(players.get(i), i);
		}
		dealerInt = ThreadLocalRandom.current().nextInt(0, numPlayer);
	}
	
	
	
	/*
	 * Public method.
	 */
	public void run() {
		while (gameActive) {
			play_round();
		}
	}
	
	/*
	 * Private methods.
	 */
	private void play_round() { 
		round++;
		turn = 0;
		tricks_bets = new int[numPlayer];
		tricks_actual = new int[numPlayer];
		printToScreen("ROUND " + round + ":");
		pause();
		dealerInt = (dealerInt + 1) % numPlayer;
		starterInt = (dealerInt + 1) % numPlayer;
		if (dealerInt == 0) {
			printToScreen("You are the dealer");
		} else {			
			printToScreen(players.get(dealerInt).getName() + " is the dealer");
		}
		deck.init();
		dealCards();
		displayCards();
		pause();
		revealTrump();
		pause();
		askTricks();
		pause();
		while (players.get(0).getCards().getSize() > 0) {
			play_turn();
		}
		concludeRound();
		pause();
		check_game_active();
	}
	
	private void play_turn() {
		turn++;
		printToScreen("TURN " + turn + ":");
		pause();
		pot.clear();
		for (int i = 0; i < numPlayer; i++) {
			int j = (i + starterInt) % numPlayer;
			Card c = players.get(j).playCard();
			pot.add(c);
			if (players.get(j) instanceof PlayerUser) {
				printToScreen("You play " + c + ".");
			} else {
				printToScreen(players.get(j).getName() + " plays " + c + ".");
			}
			pause();
		}
		determineWinner();
		pause();
	}
	
	/*
	 * OTHER METHODS.
	 */
	
	private void askTricks() {
		for (int i = 0; i < numPlayer; i++) {
			int j = (i + 1 + dealerInt) % numPlayer;
			int tricks = players.get(j).decideTricks();
			tricks_bets[j] = tricks;
			String pluralS = tricks == 1 ? "" : "s";
			if (players.get(j) instanceof PlayerUser) {
				printToScreen("You bet " + tricks + " trick" + pluralS + ".");
			} else {
				printToScreen(players.get(j).getName() + " bets " + tricks + " trick" + pluralS + ".");
			}
		}
		printToScreen("Bets: " + Arrays.toString(tricks_bets));
	}

	
	private void check_game_active() {
//			game_active = (round < 3); // for debugging.
			if (round == 60 / numPlayer) {
				gameActive = false;
			}
		}
	
	private void concludeRound() {
		printToScreen("SUMMARY:");

		int[] scores = new int[numPlayer];
		int[] player_scores = new int[numPlayer];
		for (int i = 0; i < numPlayer; i++) {
			if (tricks_bets[i] == tricks_actual[i]) {
				scores[i] = 20 + tricks_actual[i] * 10;
			} else {
				scores[i] = - Math.abs(tricks_actual[i] - tricks_bets[i]) * 10;
			}
			players.get(i).addScore(scores[i]);
			player_scores[i] = players.get(i).getScore();
		}
		printToScreen("This round:  " + Arrays.toString(scores));
		printToScreen("Total score: " + Arrays.toString(player_scores));
	}
	
	// Deal cards at the start of every round.
	private void dealCards() {
		for (int i = 0; i < numPlayer; i++) {
			players.get(i).emptyHand(); // make sure the player has no hidden cards.
			for (int j = 0; j < round; j++) { // deal them n cards each at round n.
				players.get(i).draw(deck);
			}
		}
	}

	
	private void determineWinner() {
		// if there is a wizard, the first wizard wins.
		List<Card> wizards = pot.getCards().stream().filter(c -> c.getSuit() == WIZARD).collect(Collectors.toList());
		List<Card> jesters = pot.getCards().stream().filter(c -> c.getSuit() == JESTER).collect(Collectors.toList());
		Card winning;
		if (wizards.size() > 0) {
			winning = wizards.get(0);
		} else if (jesters.size() == pot.getSize()){
			winning = jesters.get(0);
		} else {
			winning = Collections.max(pot.getCards(), cardComparator);
		}
		Player winner = winning.getOwner();
		starterInt = id.get(winner);
		tricks_actual[starterInt]++;
		if (winner instanceof PlayerUser) {
			printToScreen("You win with " + winning + ".");
		} else {
			printToScreen(winner.getName() + " wins with " + winning + ".");
		}
		printToScreen("Bets:   " + Arrays.toString(tricks_bets));
		printToScreen("Actual: " + Arrays.toString(tricks_actual));
	}
	
	private void displayCards() {
		players.get(0).getCards().sort(new CardComparator());
		printToScreen("Your cards are " + players.get(0).getCards());
		if (displayEveryonesCards) {
			for (int i = 1; i < numPlayer; i++) {
				players.get(i).getCards().sort(new CardComparator());
				printToScreen(players.get(i).getName() + "'s cards are " + players.get(i).getCards());
			}
		}
	}
	
	private void pause() {
		if (makeUserPressEnter) {
			scanner.nextLine();
		}
	}
	
	// If there are no cards left, then there is no trump suit.
	// If card is Wizard, the dealer decides the trump.
	private void revealTrump() {
		if (deck.getSize() == 0) {
			trumpCard = new Card(JESTER);
			trump = JESTER;
			printToScreen("There is no trump as there are no more cards");
		} else {
			trumpCard = deck.draw();
			printToScreen("The trump card drawn is " + trumpCard);
			if (trumpCard.getSuit() == WIZARD) {
				trump = players.get(dealerInt).decideTrump();
				printToScreen("Player " + dealerInt + " decides the trump to be " + trump.getSymbol());
			} else if (trumpCard.getSuit() == JESTER) {
				trump = JESTER;
				printToScreen("There is no trump");
			} else {
				trump = trumpCard.getSuit();
				printToScreen("Trump is " + trump.getSymbol());
			}
		}
		cardComparator = new CardComparator(trump);
	}

	
	
	/*
	 * Getters.
	 */
	public CardComparator getCardComparator() {
		return cardComparator;
	}
	
	public Deck getPot() {
		return pot;
	}
	
	public int getRound() {
		return round;
	}
	
	public Suit getTrump() {
		return trump;
	}
	
	/*
	 * Local printing method.
	 */
	private void printToScreen(Object string) {
		if (printToScreenIsOn) {
			System.out.println(string);
		}
	}
	
	
}
