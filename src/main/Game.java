package main;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import player.Player;
import player.PlayerAuto;
import player.PlayerUser;

public class Game {
	private static Scanner scanner = new Scanner(System.in);
	private List<Player> players = new ArrayList<>();
	public boolean makeUserPressEnter;
	private boolean printToScreenIsOn;
	
	/*
	 * Constructor with builder method.
	 */
	public static class Builder {
		// Optional parameters
		private boolean makeUserPressEnter = true;
		private boolean printToScreenIsOn = true;
		private final List<Player> players = new ArrayList<>();
		
		public Builder() {

		}
		
		public Builder addPlayerUser() {
			players.add(new PlayerUser());
			return this;
		}
		
		public Builder addPlayerUser(String yourName) {
			players.add(new PlayerUser(yourName));
			return this;
		}
		
		public Builder addPlayerAuto(int numPlayers) {
			for (int i = 0; i < numPlayers; i++) {
				players.add(new PlayerAuto());
			}
			return this;
		}
		
		public Builder printToScreenIsOn(boolean val) {
			printToScreenIsOn = val;
			return this;
		}
		
		public Builder makeUserPressEnter(boolean val) {
			makeUserPressEnter = val;
			return this;
		}
		
		public Game build() {
			return new Game(this);
		}
	}
	
	private Game(Builder builder) {
		players = builder.players;
		makeUserPressEnter = builder.makeUserPressEnter;
		printToScreenIsOn = builder.printToScreenIsOn;
		
		introducePlayers();
		run();
	}
	
	/*
	 * Runs a game where the user input is required.
	 * 
	 * <p>This method is to be called by the main program.
	 * 
	 * @param	none
	 * @return	none
	 * @throws	none
	 */
	public static void runUserGame() {
		new Game.Builder().addPlayerUser("Test Player").addPlayerAuto(2)
		.printToScreenIsOn(true).makeUserPressEnter(true).build();
	}
	
	
	public static void runAutoGame() {
		new Game.Builder().addPlayerAuto(6)
		.printToScreenIsOn(true).makeUserPressEnter(false).build();
	}
	
	
	

	
	/*
	 * Private methods.
	 */
	private void introducePlayers() {
		printToScreen("Here are the players.");
		for (int i = 0; i < players.size(); i++) {
			printToScreen("Player " + i + ": " + players.get(i).getName());
		}
		pause();
	}
	
	private void pause() {
		if (makeUserPressEnter) {
			scanner.nextLine();
		}
	}
	
	private void run() {
		Arena arena = new Arena.Builder(players)
				.printToScreenIsOn(printToScreenIsOn)
				.makeUserPressEnter(makeUserPressEnter)
				.build();
		for (Player player : players) {
			player.setArena(arena);
		}
		arena.run();
	}
	
	
	/*
	 * Local printing method.
	 */
	private void printToScreen(Object string) {
		if (printToScreenIsOn) {
			System.out.println(string);
		}
	}
}
