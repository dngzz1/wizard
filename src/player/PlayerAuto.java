package player;
import card.Card;
import card.Suit;
import compete.Strategy;


public class PlayerAuto extends Player{
	private Strategy strategy = new Strategy();
	/*
	 * Constructor.
	 */

	public PlayerAuto(String name) {
		this.id = countAbstractPlayer++;
		this.name = name;
	}
	
	public PlayerAuto() {
		this(NameGenerator.getRandomFirstName());
	}
	

	/*
	 * Decide how many tricks you can bet.
	 */
	public int decideTricks() {
		strategy.update(arena, cards);
		return strategy.decideTricks();
	}
	
	/*
	 * Decide card to be trump. Just take the most numerous suit.
	 * If all cards are special suits then just pick a random suit.
	 */
	public Suit decideTrump() {
		strategy.update(arena, cards);
		return strategy.decideTrump();
	}
	
	
	/*
	 * Plays a random card when asked to put down a card with no extra info given.
	 */
	public Card playCard() {
		strategy.update(arena,  cards);
		Card cardToBePlayed = strategy.playCard();
		cards.remove(cardToBePlayed);
		return cardToBePlayed;
	}

	
	

	
}



