package player;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import card.Card;
import card.Suit;

public class PlayerUser extends Player {
	Scanner scanner = new Scanner(System.in);

	/*
	 * Constructor.
	 */
	public PlayerUser() {
		this.id = countAbstractPlayer++; // automatically add id.
		this.name = NameGenerator.getRandomFirstName();
	}
	
	public PlayerUser(String name) {
		this.id = countAbstractPlayer++;
		this.name = name;
	}
	
	/*
	 * Implementing abstract methods.
	 */
	public int decideTricks() {
		int num_tricks = -1;
		do {
			System.out.println("Enter the number of tricks to bet (between 0 and " + arena.getRound() + "):");
			sortCards();
			printUnnumberedCards();
			num_tricks = scanner.nextInt();
		} while (num_tricks < 0 || num_tricks > arena.getRound());
		return num_tricks;
	}
	
	@Override
	public Suit decideTrump() {
		char t;
		do {
			System.out.println("Which suit would you like to be trump? C for ♣, D for ♦, H for ♥, S for ♠");
			t = scanner.next().charAt(0);
			t = Character.toUpperCase(t);
		} while (t != 'C' && t!= 'D' && t != 'H' && t != 'S');
		switch (t) {
			case 'C':
				return Suit.CLUB;
			case 'D':
				return Suit.DIAMOND;
			case 'H':
				return Suit.HEART;
			default:
				return Suit.SPADE;
		}
	}
	
	@Override
	public Card playCard() {
		// sort card before displaying.
		sortCards();
		
//		// if there is one card left then just play it.
//		if (cards.getSize()== 1) {
//			return cards.remove(0);
//		}
		
		int input = -1;
		List<Card> regCardsInPot = arena.getPot().getCards().stream()
				.filter(c -> !c.getSuit().getIsSpecial())
				.collect(Collectors.toList());
		if (regCardsInPot.size() == 0) {
			// no regular suited card is in the pot.
			// hence no restrictions on card to be played.
			do {
				System.out.println("Enter the integer you wish to play");
				printNumberedCards();
				input = scanner.nextInt();
			} while (input < 0 || input >= cards.getSize());
			return cards.remove(input);
		} 
		// else someone played a regular suited card.
		// check whether we have any of the same suit.
		Suit leadSuit = regCardsInPot.get(0).getSuit();
		List<Card> cardsOfSameSuit = cards.getCards().stream()
				.filter(c -> c.getSuit() == leadSuit)
				.collect(Collectors.toList());
		if (cardsOfSameSuit.size() == 0) {
			// we don't have the same suit, so no restrictions.
			do {
				System.out.println("Enter the integer you wish to play");
				printNumberedCards();
				input = scanner.nextInt();
			} while (input < 0 || input >= cards.getSize());
			return cards.remove(input);
		}
		// else must follow suit, or play a special card.
		// create numbering system for playable cards.
		List<Card> playables = cards.getCards().stream()
				.filter(c -> c.getSuit().getIsSpecial()
				|| c.getSuit() == leadSuit)
				.sorted(arena.getCardComparator())
				.collect(Collectors.toList());
		List<Card> unplayables = cards.getCards().stream()
				.filter(c -> !c.getSuit().getIsSpecial()
				&& c.getSuit() != leadSuit)
				.sorted(arena.getCardComparator())
				.collect(Collectors.toList());
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < playables.size(); i++) {
			sb.append(i + ":");
			sb.append(playables.get(i));
			if (i < playables.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		sb.append("[");
		for (int i = 0; i < unplayables.size(); i++) {
			sb.append("X:");
			sb.append(unplayables.get(i));
			if (i < unplayables.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		// ask user input.
		do {
			System.out.println("Enter the integer you wish to play");
			System.out.println(sb.toString());
			input = scanner.nextInt();
		} while (input < 0 || input >= playables.size());
		cards.remove(playables.get(input));
		return playables.get(input);
		
		
	}
	

	
	

	
}
