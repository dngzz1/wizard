package player;
import java.util.Collections;
import java.util.List;

import card.Card;
import card.Deck;
import card.Suit;
import main.Arena;


public abstract class Player implements Comparable<Player>{
	Deck cards = new Deck();
	static int countAbstractPlayer = 0;
	int id;
	Arena arena;
	String name;
	int score = 0;
	
	


	
	

	
	/*
	 * Decide how many tricks you can bet.
	 */
	public abstract int decideTricks();
	
	/*
	 * Decide card to be trump. Just take the most numerous suit.
	 * If all cards are special suits then just pick a random suit.
	 */
	public abstract Suit decideTrump();
	
	/*
	 * Player takes a card and signs it with their name.
	 */
	public void draw(Deck deck) {
		Card drawn_card = deck.draw();
		drawn_card.setOwner(this);
		cards.add(drawn_card);
	}
	
	/*
	 * Empty the player's hand.
	 */
	public void emptyHand() {
		cards.clear();
	}
	
	
	/*
	 * Plays a random card when asked to put down a card with no extra info given.
	 */
	public abstract Card playCard();
	
	void sortCards() {
		Collections.sort(cards.getCards(), arena.getCardComparator());
	}
	
	
	/*
	 * Getters.
	 */
	public Deck getCards() {
		return cards;
	}
	
	
	
	public String getName() {
		return name;
	}
	

	
	public int getScore() {
		return score;
	}

	/*
	 * Setters.
	 */
	public void setArena(Arena arena) {
		this.arena = arena;
	}
	
	public void addScore(int s) {
		score += s;
	}
	
	/*
	 * Print.
	 */
	@Override
	public String toString() {
		return name + "(id=" + id + ")";
	}
	
	public void printNumberedCards() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < cards.getSize(); i++) {
			sb.append(i);
			sb.append(":");
			Card c = cards.get(i);
			sb.append(c);
			if (c.getSuit() == arena.getTrump()) {
				sb.append("*");
			}
			if (i < cards.getSize() - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		System.out.println(sb);
	}
	
	public void printUnnumberedCards() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < cards.getSize(); i++) {
			Card c = cards.get(i);
			sb.append(c);
			if (c.getSuit() == arena.getTrump()) {
				sb.append("*");
			}
			if (i < cards.getSize() - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");
		System.out.println(sb);
	}


	@Override
	public int compareTo(Player a) {
		return Integer.compare(getScore(), a.getScore());
	}
	

	
}



